# Resize Image

1. Create a **IAM Role**
    - Go to [IAM console](https://console.aws.amazon.com/iam/home#/roles)
    - **Create role -> Lambda -> Next: Permission -> Create policy**, a new tab should open
    - On that tab, choose JSON and add
    ```json
    {
        "Version": "2012-10-17",
        "Statement": [
            {
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents"
            ],
            "Resource": "arn:aws:logs:*:*:*"
            },
            {
                "Effect": "Allow",
                "Action": "s3:PutObject",
                "Resource": "arn:aws:s3:::__BUCKET_NAME__/*"
            }
        ]
    }
    ```
    - Click **Review policy**
    - Name your policy, for example: *"access_to_putObject_policy"* and click on **Create policy**; you can close the tab
    - On the previous tab, update the policy list clicking on the button with reload image or reloading the page.
    - Select your policy clicking on the checkbox
    - Click on **Next: tags -> Next: Review**, name your role, for example, *"access_to_putObject_role"*
    - Click on **Create role**; you can close the tab.

2. Create a **Lambda**
   * Create function
        - Go to [Services -> Compute -> Lambda](https://console.aws.amazon.com/lambda/home)
        - **Create a function -> Author from scratch**
        - Enter a name (e.g. s3-resizer)
        - Select the latest version of Node.js 12.x
        - Click **Choose or create an execution role** to show additional info
        - Check **Use an existing role** and choose your role in the list, update the list if necessary.
        - After clicking on **Create function**, the lambda should be created.
   * Add a trigger, which will listen to http requests
        - **YOUR_LAMBDA -> Add trigger -> API Gateway**
        - **Create a new API**
        - In **Security**, select **Open**, then click **Add**
        - Click on **API Gateway**, you should see **API endpoint**, something like `https://some-id.execute-api.eu-central-1.amazonaws.com/your-stage/your-lambdas-name`

3. Set up Static website hosting
    * Go to [Services -> Storage -> S3](https://s3.console.aws.amazon.com/s3/home)
    * Add permission
        - **YOUR_BUCKET -> Permissions -> Bucket policy** and paste
        ```json
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "AddPerm",
                    "Effect": "Allow",
                    "Principal": "*",
                    "Action": "s3:GetObject",
                    "Resource": "arn:aws:s3:::__BUCKET_NAME__/*"
                }
            ]
        }
        ```
        - Click **Save**
    * Add static website hosting
        - Go to **Properties (next to Permissions) -> Static website hosting -> Select "Use this bucket to host a website"**
        - In **Index document** type "index.html"
        - Paste that **Redicrection rules**
        ```xml
        <RoutingRules>
            <RoutingRule>
                <Condition>
                    <KeyPrefixEquals/>
                    <HttpErrorCodeReturnedEquals>404</HttpErrorCodeReturnedEquals>
                </Condition>
                <Redirect>
                    <Protocol>https</Protocol>
                    <HostName>__DOMAIN__</HostName>
                    <ReplaceKeyPrefixWith>__PATH_TO_LAMBDA__?path=</ReplaceKeyPrefixWith>
                    <HttpRedirectCode>307</HttpRedirectCode>
                </Redirect>
            </RoutingRule>
        </RoutingRules>
        ```
        > Pay attention to `__DOMAIN__` and `__PATH_TO_LAMBDA__` (protocol is always _https_)  
        > This is your **API endpoint**. For example, if the url is `https://some-id.execute-api.us-east-1.amazonaws.com/your-stage/your-lambdas-name`, the correct xml nodes shall look like
        ```xml
        <HostName>some-id.execute-api.us-east-1.amazonaws.com</HostName>
        <ReplaceKeyPrefixWith>your-stage/your-lambdas-name?path=</ReplaceKeyPrefixWith>
        ```
        - At this state, before clicking on **Save**, copy your **Endpoint**. Do not mix it up. This is an endpoint of your Static website hosting, and it is http, not https.

4. Make lambda work
    * Download [function.zip]('./../dist/function.zip') in _dist/function.zip_ and make lambda work
        - **Function code -> Code entry type -> Upload a .zip file** upload a zip file
        - Click **Save**
        - Set up these two **Environment variables** _(format: key=value)_ 
        **BUCKET**=_your bucket 's name_
        **URL**=**Endpoint** (from Static website hosting)
        In **Basic settings**
            Allocate 768mb memory
            Timeout could be 5 seconds
        - **Save** it. You are done!

    * Test your lambda (optional)
        - Go to lambda, click on **Test**, and paste this json:
        ```json
        {
            "queryStringParameters": {"path": __YOUR_IMAGE_PATH_WITH_SIZE_PREFIX__}
        }
        ```
        - for example: `150x150/pretty_image.jpg`
        -  Go back to the bucket, a new directory _150x150_ must be created

5. Use
   - Upload an image to your bucket and copy the full path to it. Check whether the image shows in your browser entering **"ENDPOINT/FULL_PATH"**; **Endpoint**  is your Static website hosting (http)
   - Instead of `WxH` there're some extra available _magic paths_:  
    `.../AUTOx150/image.jpg`  
    `.../150xAUTO/image.jpg`